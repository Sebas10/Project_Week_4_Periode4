﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseGame : MonoBehaviour
{

    private bool AmIPaused = false;

    [SerializeField] private GameObject PausedObjects;

    // Use this for initialization
    void Start()
    {
        PausedObjects.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            AmIPaused = !AmIPaused;
        }
        if (AmIPaused)
        {
            PausedObjects.SetActive(true);
            if (Input.GetKeyDown(KeyCode.C))
            {
                AmIPaused = false;
            }
            if (Input.GetKeyDown(KeyCode.M))
            {
                SceneManager.LoadScene(0);
            }
        }
        else
        {
            PausedObjects.SetActive(false);
        }
    }
    public void Continue()
    {
        AmIPaused = false;

    }
}
