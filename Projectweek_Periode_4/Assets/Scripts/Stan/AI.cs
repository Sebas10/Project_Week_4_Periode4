﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    [SerializeField] private Transform _Target;
    [SerializeField] private NavMeshAgent _NPC;
    [SerializeField] private Transform _Player;

    [SerializeField] private Animator m_Animator;

    [SerializeField] private string m_WalkingAnimTriggerName;

    public bool m_IdleOn = true, m_Sate = false;

	void Update ()
    {
        if (m_IdleOn)
        {
            m_Animator.SetTrigger(m_WalkingAnimTriggerName);
            _Target.position = new Vector3(0.7102944f, -1.949122f, -2.87f);
            _NPC.SetDestination(_Target.position);
            Vector3 targetPostition = new Vector3(_Target.position.x, this.transform.position.y, _Target.position.z);
            this.transform.LookAt(targetPostition);
        }
        else
        {
            _Target.position = new Vector3(0.7102944f, -1.949122f, -2.87f);            
            _NPC.SetDestination(_Target.position);
            Vector3 targetPostition = new Vector3(_Player.position.x, this.transform.position.y, _Player.position.z);
            this.transform.LookAt(targetPostition);
        }

        if (m_Sate)
        {
            m_Animator.SetTrigger(m_WalkingAnimTriggerName);
            _Target.position = new Vector3(-10.3f, -1.949122f, -14.94f);
            _NPC.SetDestination(_Target.position);
            Vector3 targetPostition = new Vector3(_Target.position.x, this.transform.position.y, _Target.position.z);
            this.transform.LookAt(targetPostition);
        }
        
	}
}
