﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkingAI : MonoBehaviour
{
    public string _String;
    public Text _Text;
    public bool _DisplayText;

    [SerializeField]
    private GameObject m_Image;
 
    public int _TextNumber;
    public int _StartingText;
    void Start ()
    {
        _StartingText = Random.Range(0, 3);
    }
	
	void Update ()
    {
        DisplayText();
        #region Gun Questions
        if (_TextNumber == 0)
        {
            _String = "I'll have \n1 .50 Cal please";
        }
        else if (_TextNumber == 1)
        {
            _String = "i'll have \n1 Knife please";
        }
        else if (_TextNumber == 2)
        {
            _String = "i'll have \n1 SKS please";
        }
        else if (_TextNumber == 3)
        {
            _String = "i'll have \n1 1911 please";
        }
        else if (_TextNumber == 4)
        {
            _String = "i'll have \n1 AK47 please";
        }
        else if (_TextNumber == 5)
        {
            _String = "i'll have \n1 Boxing Knife please";
        }
        else if (_TextNumber == 6)
        {
            _String = "I'll have \n1 Glock please";
        }
        else if (_TextNumber == 7)
        {
            _String = "i'll have \n1 Crossbow please";
        }
        else if (_TextNumber == 8)
        {
            _String = "i'll have \n1 Grenade please";
        }
        else if (_TextNumber == 9)
        {
            _String = "i'll have \n1 Kar98 please";
        }
        else if (_TextNumber == 10)
        {
            _String = "i'll have \n1 M60e4 please";
        }
        else if (_TextNumber == 11)
        {
            _String = "i'll have \n1 Remington please";
        }
        else if (_TextNumber == 12)
        {
            _String = "i'll have \n1 Spas-12 please";
        }
        else if (_TextNumber == 13)
        {
            _String = "i'll have \n1 Zombie Knife please";
        }
#endregion

        if(_StartingText == 0)
        {
            _String = "Ghello, I am from Albania, \nI need gun for kill";
        }
        else if (_StartingText == 1)
        {
            _String = "I from Belarus, \nYou give me gun";
        }
        else if(_StartingText == 2)
        {
            _String = "Hello im from bulgaria,\n you give gun me yes?";
        }
        else if(_StartingText == 3)
        {
            _String = "Hello I is from \nRomania and i want a gun";
        }
        else if(_StartingText == 4)
        {
            // do nothing
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            _StartingText = 4;
            _TextNumber = Random.Range(0, 13);
        }
    }

    private void DisplayText()
    {
        if (_DisplayText == true)
        {
            m_Image.SetActive(true);
            _Text.text = _String;
        }

        else
        {
            m_Image.SetActive(false);
            _Text.text = "";
        }
    }
}
