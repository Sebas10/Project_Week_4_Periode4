﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunUnlocks : MonoBehaviour
{
    [SerializeField] private GameObject _Gun;
    [SerializeField] private GameObject _NormalGun;
    [SerializeField] private Money m_Money;
    [SerializeField] private int _Price;

    private void OnMouseOver()
    {
        if(m_Money.m_CurrentMoney >= _Price)
        {
            if (Input.GetKeyDown(KeyCode.B))
            {
                _NormalGun.SetActive(true);
                m_Money.ItemBaught(_Price);
                Destroy(_Gun);
                Destroy(gameObject);
                m_Money.AmIDead();
            }
        }
    }
}
