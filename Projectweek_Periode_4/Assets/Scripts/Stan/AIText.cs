﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIText : MonoBehaviour
{
    [SerializeField] private TalkingAI AITalking;

    private void OnTriggerStay(Collider _AI)
    {
        if(_AI.CompareTag("Enemy"))
        {
            AITalking._DisplayText = true;
        }
    }
    private void OnTriggerExit(Collider _AI)
    {
        if(_AI.CompareTag("Enemy"))
        {
            AITalking._DisplayText = false;
        }
    }
}
