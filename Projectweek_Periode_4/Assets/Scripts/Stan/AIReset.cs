﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIReset : MonoBehaviour
{
    [SerializeField] private Transform _AiStart;
    [SerializeField] private Transform _NextDestination;
    [SerializeField] private Transform _NewDestination;
    [SerializeField] private Transform _NPC;
    [SerializeField] private NavMeshAgent _AI;
    [SerializeField] private TalkingAI _AIText;
    [SerializeField] private Collider _ColliderAI;
    [SerializeField] private TypeOfBuyer m_TOB;
    [SerializeField] private Money m_Money;

    [SerializeField]
    private AI m_AIScript;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            m_AIScript.m_Sate = true;
            m_AIScript.m_IdleOn = false;
        }
    }

    private void OnTriggerEnter(Collider _ColliderAI)
    {
        if (_ColliderAI.CompareTag("Enemy"))
        {
            m_AIScript.m_IdleOn = true;
            m_AIScript.m_Sate = false;
            m_TOB.NieuwGetal();
            m_Money.CashIn(1000);
            _NPC.transform.position = _AiStart.transform.position;
            _AI.SetDestination(_NextDestination.position);
            _AIText._StartingText = Random.Range(0, 3);
            _AIText._TextNumber = Random.Range(0, 13);
        }  
    }
}
