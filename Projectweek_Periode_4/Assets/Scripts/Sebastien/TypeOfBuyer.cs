﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypeOfBuyer : MonoBehaviour
{
    public int caseSwitch;

    [SerializeField] private GameObject[] m_buyers;


    private void Start()
    {
        caseSwitch = Random.Range(0, 5);
    }

    // Update is called once per frame
    void Update()
    {
        switch (caseSwitch)
        {
            case 0:
                m_buyers[0].SetActive(true);
                m_buyers[1].SetActive(false);
                break;
            case 1:
                m_buyers[0].SetActive(false);
                m_buyers[1].SetActive(true);
                break;
            case 2:
                m_buyers[0].SetActive(true);
                m_buyers[1].SetActive(false);
                break;
            case 3:
                m_buyers[0].SetActive(false);
                m_buyers[1].SetActive(true);
                break;
            case 4:
                m_buyers[0].SetActive(true);
                m_buyers[1].SetActive(false);
                break;
            case 5:
                m_buyers[0].SetActive(false);
                m_buyers[1].SetActive(true);
                break;
            default:
                caseSwitch = 0;
                break;
        }
    }

    public void NieuwGetal()
    {
        caseSwitch = Random.Range(0, 5);
    }
}
