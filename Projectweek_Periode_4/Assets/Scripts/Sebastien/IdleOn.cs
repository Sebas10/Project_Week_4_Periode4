﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleOn : MonoBehaviour
{
    [SerializeField]
    private AI m_AI;
    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            m_AI.m_IdleOn = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            m_AI.m_IdleOn = false;
        }
    }
}
