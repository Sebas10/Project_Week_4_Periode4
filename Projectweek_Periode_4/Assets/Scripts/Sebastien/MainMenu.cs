﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject m_ControllPanel, m_MainMenu;

    private void Start()
    {
        m_ControllPanel.SetActive(false);
        m_MainMenu.SetActive(true);
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void SetControllsOn()
    {
        m_ControllPanel.SetActive(true);
        m_MainMenu.SetActive(false);
    }
    public void SetControllsOff()
    {
        m_MainMenu.SetActive(true);
        m_ControllPanel.SetActive(false);
    }
}
