﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Money : MonoBehaviour
{

    public int m_CurrentMoney;

    [SerializeField]
    private Text m_MoneyText;

    // Use this for initialization
    void Start()
    {
        m_CurrentMoney = 500;
        m_MoneyText.text = m_CurrentMoney.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.H))
        {
            CashIn(20);
        }        
    }

    public void CashIn(int amount)
    {
        m_CurrentMoney += amount;
        m_MoneyText.text = m_CurrentMoney.ToString();
    }

    public void ItemBaught(int amount)
    {
        m_CurrentMoney -= amount;
        m_MoneyText.text = m_CurrentMoney.ToString();
    }
    public void AmIDead()
    {
        if (m_CurrentMoney <= 1)
        {
            SceneManager.LoadScene(2);
        }
    }
}
