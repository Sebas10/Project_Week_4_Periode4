﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    private RaycastHit m_raycastHit;
    [SerializeField] private Transform m_BarrelEnd;

    [SerializeField]
    private Animator m_ShootingAnimator;

    [SerializeField]
    private AudioSource m_GunShot;

    [SerializeField]
    private GameObject m_GunObject;

    [SerializeField]
    private ParticleSystem m_ShootingParticle;

    private bool m_WeaponOn = false;

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Q))
        {
            m_WeaponOn = !m_WeaponOn;
        }
        if (m_WeaponOn)
        {
            m_GunObject.SetActive(true);
        }
        else
        {
            m_GunObject.SetActive(false);
            return;
        }


        if (Input.GetMouseButtonDown(0))
        {
            m_ShootingAnimator.SetTrigger("ShootTrigger");
            m_GunShot.Play();
            m_ShootingParticle.Play();
            if (Physics.Raycast(m_BarrelEnd.position, m_BarrelEnd.forward, out m_raycastHit, 10f))
            {
                if (m_raycastHit.collider.tag == "Enemy")
                {
                    m_raycastHit.transform.gameObject.GetComponent<EnemyHealth>().TakeDamage();
                }
            }

        }
    }
}
