﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField]
    private Transform m_RespawnPoint;

    [SerializeField]
    private TypeOfBuyer m_TypeBuyer;

    [SerializeField]
    private Money m_Money;

    public void TakeDamage()
    {
        m_TypeBuyer.NieuwGetal();
        m_Money.ItemBaught(1500);
        m_Money.AmIDead();
        transform.position = m_RespawnPoint.position;
    }
}
