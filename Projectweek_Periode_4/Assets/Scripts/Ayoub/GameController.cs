﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    [SerializeField]
    private GameObject m_AK47;
    [SerializeField]
    private GameObject m_Glock;
    [SerializeField]
    private GameObject m_SPAS12;
    [SerializeField]
    private GameObject m_SKS;
    [SerializeField]
    private GameObject m_LMG;
    [SerializeField]
    private GameObject m_Remington;
    [SerializeField]
    private GameObject m_BoxingKnife;
    [SerializeField]
    private GameObject m_Kar98k;
    [SerializeField]
    private GameObject m_ZombieKnife;
    [SerializeField]
    private GameObject m_Grenade;
    [SerializeField]
    private GameObject m_Crossbow;
    [SerializeField]
    private GameObject m_50CAL;

    [SerializeField]
    private List<GameObject> m_AKList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_GlockList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_SPASList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_SKSList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_LMGList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_RemingtonList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_BoxKnifeList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_Kar98kList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_ZombieKnifeList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_GrenadeList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_CrossbowList = new List<GameObject>();
    [SerializeField]
    private List<GameObject> m_50CALList = new List<GameObject>();

    [SerializeField]
    private Transform m_AKBuyPoint;
    [SerializeField]
    private Transform m_GlockBuyPoint;
    [SerializeField]
    private Transform m_SPASBuyPoint;
    [SerializeField]
    private Transform m_SKSBuyPoint;
    [SerializeField]
    private Transform m_LMGBuyPoint;
    [SerializeField]
    private Transform m_RemingtonBuyPoint;
    [SerializeField]
    private Transform m_BoxKnifeBuyPoint;
    [SerializeField]
    private Transform m_Kar98kBuyPoint;
    [SerializeField]
    private Transform m_ZombieKnifeBuyPoint;
    [SerializeField]
    private Transform m_GrenadeBuyPoint;
    [SerializeField]
    private Transform m_CrossbowBuyPoint;
    [SerializeField]
    private Transform m_50CALBuyPoint;

    [SerializeField]
    private Transform m_DeskPos1;
    [SerializeField]
    private Transform m_DeskPos2;
    [SerializeField]
    private Transform m_DeskPos3;

    [SerializeField]
    private int m_AKPrice;
    [SerializeField]
    private int m_GlockPrice;
    [SerializeField]
    private int m_SPASPrice;
    [SerializeField]
    private int m_SKSPrice;
    [SerializeField]
    private int m_LMGPrice;
    [SerializeField]
    private int m_RemingtonPrice;
    [SerializeField]
    private int m_BoxKnifePrice;
    [SerializeField]
    private int m_Kar98kPrice;
    [SerializeField]
    private int m_ZombieKnifePrice;
    [SerializeField]
    private int m_GrenadePrice;
    [SerializeField]
    private int m_CrossbowPrice;
    [SerializeField]
    private int m_50CALPrice;

    [SerializeField]
    private ToolTip m_AKText;
    [SerializeField]
    private ToolTip m_GlockText;
    [SerializeField]
    private ToolTip m_SPASText;
    [SerializeField]
    private ToolTip m_SKSText;
    [SerializeField]
    private ToolTip m_LMGText;
    [SerializeField]
    private ToolTip m_RemingtonText;
    [SerializeField]
    private ToolTip m_BoxKnifeText;
    [SerializeField]
    private ToolTip m_Kar98kText;
    [SerializeField]
    private ToolTip m_ZombieKnifeText;
    [SerializeField]
    private ToolTip m_GrenadeText;
    [SerializeField]
    private ToolTip m_CrossbowText;
    [SerializeField]
    private ToolTip m_50CALText;

    private Vector3 m_Pos1;
    private Vector3 m_Pos2;
    private Vector3 m_Pos3;

    private bool m_Pos1Taken;
    private bool m_Pos2Taken;
    private bool m_Pos3Taken;

    private RaycastHit hit;

    [SerializeField]
    private Camera m_Camera;

    [SerializeField]
    private Money m_Money;

    // Use this for initialization
    void Start ()
    {
        m_Pos1 = new Vector3(m_DeskPos1.transform.position.x, m_DeskPos1.transform.position.y, m_DeskPos1.transform.position.z);
        m_Pos2 = new Vector3(m_DeskPos2.transform.position.x, m_DeskPos2.transform.position.y, m_DeskPos2.transform.position.z);
        m_Pos3 = new Vector3(m_DeskPos3.transform.position.x, m_DeskPos3.transform.position.y, m_DeskPos3.transform.position.z);

        m_AKList.Add(m_AK47);
        m_AKList.Add(m_AK47);
        m_AKList.Add(m_AK47);

        m_CrossbowList.Add(m_Crossbow);
        m_CrossbowList.Add(m_Crossbow);
        m_CrossbowList.Add(m_Crossbow);
    }
	
	// Update is called once per frame
	void Update ()
    {
        CheckWeaponOnRack();

        CheckWeaponOnDesk();

        BuyGunInStorage();
    }

    private void LateUpdate()
    {
        AmountOfWeapons();
    }

    private void CheckWeaponOnRack()
    {
        float reach = 3;

        Ray camera = m_Camera.ScreenPointToRay(Input.mousePosition);

        Debug.DrawRay(m_Camera.transform.position, m_Camera.transform.forward * reach, Color.red);

        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward * reach, out hit, reach))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (hit.transform.tag == "AK47")
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    if (hit.transform.position == m_AKBuyPoint.position)
                    {
                        if (m_AKList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_AK47, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_AKList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_AK47, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_AKList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_AK47, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_AKList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "Glock")
                {
                    if (hit.transform.position == m_GlockBuyPoint.position)
                    {
                        if (m_GlockList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_Glock, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_GlockList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_Glock, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_GlockList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_Glock, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_GlockList.Remove(m_Glock);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "SPAS12")
                {
                    if (hit.transform.position == m_SPASBuyPoint.position)
                    {
                        if (m_SPASList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_SPAS12, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_SPASList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_SPAS12, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_SPASList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_SPAS12, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_SPASList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "SKS")
                {
                    if (hit.transform.position == m_SKSBuyPoint.position)
                    {
                        if (m_SKSList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_SKS, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_SKSList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_SKS, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_SKSList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_SKS, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_SKSList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "LMG")
                {
                    if (hit.transform.position == m_LMGBuyPoint.position)
                    {
                        if (m_LMGList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_LMG, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_LMGList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_LMG, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_LMGList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_LMG, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_LMGList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "Remington")
                {
                    if (hit.transform.position == m_RemingtonBuyPoint.position) {
                        if (m_RemingtonList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_Remington, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_RemingtonList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_Remington, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_RemingtonList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_Remington, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_RemingtonList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "BoxingKnife")
                {
                    if (hit.transform.position == m_BoxKnifeBuyPoint.position)
                    {
                        if (m_BoxKnifeList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_BoxingKnife, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_BoxKnifeList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_BoxingKnife, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_BoxKnifeList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_BoxingKnife, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_BoxKnifeList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "Kar98k")
                {
                    if (hit.transform.position == m_Kar98kBuyPoint.position)
                    {
                        if (m_Kar98kList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_Kar98k, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_Kar98kList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_Kar98k, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_Kar98kList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_Kar98k, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_Kar98kList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "ZombieKnife")
                {
                    if (hit.transform.position == m_ZombieKnifeBuyPoint.position)
                    {
                        if (m_ZombieKnifeList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_ZombieKnife, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_ZombieKnifeList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_ZombieKnife, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_ZombieKnifeList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_ZombieKnife, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_ZombieKnifeList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "Grenade")
                {
                    if (hit.transform.position == m_GrenadeBuyPoint.position)
                    {
                        if (m_GrenadeList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_Grenade, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_GrenadeList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_Grenade, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_GrenadeList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_Grenade, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_GrenadeList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "Crossbow")
                {
                    if(hit.transform.position == m_CrossbowBuyPoint.position)
                    {
                        if (m_CrossbowList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_Crossbow, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_CrossbowList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_Crossbow, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_CrossbowList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_Crossbow, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_CrossbowList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }
                else if (hit.transform.tag == "50CAL")
                {
                    if (hit.transform.position == m_50CALBuyPoint.position)
                    {
                        if (m_50CALList.Count > 0)
                        {
                            if (m_Pos1Taken == false)
                            {
                                Instantiate(m_50CAL, m_Pos1, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_50CALList.RemoveAt(0);

                                m_Pos1Taken = true;
                            }
                            else if (m_Pos2Taken == false)
                            {
                                Instantiate(m_50CAL, m_Pos2, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_50CALList.RemoveAt(0);

                                m_Pos2Taken = true;
                            }
                            else if (m_Pos3Taken == false)
                            {
                                Instantiate(m_50CAL, m_Pos3, Quaternion.LookRotation(Vector3.forward, Vector3.up));

                                m_50CALList.RemoveAt(0);

                                m_Pos3Taken = true;
                            }
                        }
                    }
                }

                if (m_Pos1Taken == true)
                {
                    Debug.Log("Position 1 is taken");
                }
                if (m_Pos2Taken == true)
                {
                    Debug.Log("Position 2 is taken");
                }
                if (m_Pos3Taken == true)
                {
                    Debug.Log("Position 3 is taken");
                }
            }
        }
        else
        {
            Debug.Log("Wait a sec... This aint no weapon!");
        }
    }

    private void CheckWeaponOnDesk()
    {
        float reach = 3;

        Ray camera = m_Camera.ScreenPointToRay(Input.mousePosition);

        Debug.DrawRay(m_Camera.transform.position, m_Camera.transform.forward * reach, Color.red);

        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward * reach, out hit, reach))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (hit.transform.position == m_Pos1)
                {
                    m_Pos1Taken = false;

                    Debug.Log("Position 1 is free");
                }
                else if (hit.transform.position == m_Pos2)
                {
                    m_Pos2Taken = false;

                    Debug.Log("Position 2 is free");
                }
                else if (hit.transform.position == m_Pos3)
                {
                    m_Pos3Taken = false;

                    Debug.Log("Position 3 is free");
                }

                if (hit.transform.tag == "AK47" && !m_AKList.Contains(hit.transform.gameObject) && hit.transform.position != m_AKBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_AKList.Add(m_AK47);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "Glock" && !m_GlockList.Contains(hit.transform.gameObject) && hit.transform.position != m_GlockBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_GlockList.Add(m_Glock);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "SPAS12" && !m_SPASList.Contains(hit.transform.gameObject) && hit.transform.position != m_SPASBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_SPASList.Add(m_SPAS12);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "SKS" && !m_SKSList.Contains(hit.transform.gameObject) && hit.transform.position != m_SKSBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_SKSList.Add(m_SKS);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "LMG" && !m_LMGList.Contains(hit.transform.gameObject) && hit.transform.position != m_LMGBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_LMGList.Add(m_LMG);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "Remington" && !m_RemingtonList.Contains(hit.transform.gameObject) && hit.transform.position != m_RemingtonBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_RemingtonList.Add(m_Remington);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "BoxingKnife" && !m_BoxKnifeList.Contains(hit.transform.gameObject) && hit.transform.position != m_BoxKnifeBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_BoxKnifeList.Add(m_BoxingKnife);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "Kar98k" && !m_Kar98kList.Contains(hit.transform.gameObject) && hit.transform.position != m_Kar98kBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_Kar98kList.Add(m_Kar98k);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "ZombieKnife" && !m_ZombieKnifeList.Contains(hit.transform.gameObject) && hit.transform.position != m_ZombieKnifeBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_ZombieKnifeList.Add(m_ZombieKnife);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "Grenade" && !m_GrenadeList.Contains(hit.transform.gameObject) && hit.transform.position != m_GrenadeBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_GrenadeList.Add(m_Grenade);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "Crossbow" && !m_CrossbowList.Contains(hit.transform.gameObject) && hit.transform.position != m_CrossbowBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_CrossbowList.Add(m_Crossbow);

                    Destroy(hit.transform.gameObject);
                }
                if (hit.transform.tag == "50CAL" && !m_50CALList.Contains(hit.transform.gameObject) && hit.transform.position != m_50CALBuyPoint.position)
                {
                    Debug.Log("You've found weapon " + hit.transform.tag);

                    m_50CALList.Add(m_50CAL);

                    Destroy(hit.transform.gameObject);
                }
            }
        }
    }

    private void BuyGunInStorage()
    {
        float reach = 3;

        Ray camera = m_Camera.ScreenPointToRay(Input.mousePosition);

        Debug.DrawRay(m_Camera.transform.position, m_Camera.transform.forward * reach, Color.red);

        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward * reach, out hit, reach))
        {
            if (Input.GetKeyDown(KeyCode.B))
            {
                if (m_Money.m_CurrentMoney >= m_AKPrice)
                {
                    if (hit.transform.tag == "AK47" && !m_AKList.Contains(hit.transform.gameObject))
                    {
                        m_AKList.Add(m_AK47);
                        m_Money.ItemBaught(m_AKPrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_GlockPrice)
                {
                    if (hit.transform.tag == "Glock" && hit.transform.position == m_GlockBuyPoint.position)
                    {
                        m_GlockList.Add(m_Glock);
                        m_Money.ItemBaught(m_GlockPrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_SPASPrice)
                {
                    if (hit.transform.tag == "SPAS12" && hit.transform.position == m_SPASBuyPoint.position)
                    {
                        m_SPASList.Add(m_SPAS12);
                        m_Money.ItemBaught(m_SPASPrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_SKSPrice)
                {
                    if (hit.transform.tag == "SKS" && hit.transform.position == m_SKSBuyPoint.position)
                    {
                        m_SKSList.Add(m_SKS);
                        m_Money.ItemBaught(m_SKSPrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_LMGPrice)
                {
                    if (hit.transform.tag == "LMG" && hit.transform.position == m_LMGBuyPoint.position)
                    {
                        m_LMGList.Add(m_LMG);
                        m_Money.ItemBaught(m_LMGPrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_RemingtonPrice)
                {
                    if (hit.transform.tag == "Remington" && hit.transform.position == m_RemingtonBuyPoint.position)
                    {
                        m_RemingtonList.Add(m_Remington);
                        m_Money.ItemBaught(m_RemingtonPrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_BoxKnifePrice)
                {
                    if (hit.transform.tag == "BoxingKnife" && hit.transform.position == m_BoxKnifeBuyPoint.position)
                    {
                        m_BoxKnifeList.Add(m_BoxingKnife);
                        m_Money.ItemBaught(m_BoxKnifePrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_Kar98kPrice)
                {
                    if (hit.transform.tag == "Kar98k" && hit.transform.position == m_Kar98kBuyPoint.position)
                    {
                        m_Kar98kList.Add(m_Kar98k);
                        m_Money.ItemBaught(m_Kar98kPrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_ZombieKnifePrice)
                {
                    if (hit.transform.tag == "ZombieKnife" && hit.transform.position == m_ZombieKnifeBuyPoint.position)
                    {
                        m_ZombieKnifeList.Add(m_ZombieKnife);
                        m_Money.ItemBaught(m_ZombieKnifePrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_GrenadePrice)
                {
                    if (hit.transform.tag == "Grenade" && hit.transform.position == m_GrenadeBuyPoint.position)
                    {
                        m_GrenadeList.Add(m_Grenade);
                        m_Money.ItemBaught(m_GrenadePrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_CrossbowPrice)
                {
                    if (hit.transform.tag == "Crossbow" && hit.transform.position == m_CrossbowBuyPoint.position)
                    {
                        m_CrossbowList.Add(m_Crossbow);
                        m_Money.ItemBaught(m_CrossbowPrice);
                    }
                }
                if (m_Money.m_CurrentMoney >= m_50CALPrice)
                {
                    if (hit.transform.tag == "50CAL" && hit.transform.position == m_50CALBuyPoint.position)
                    {
                        m_50CALList.Add(m_50CAL);
                        m_Money.ItemBaught(m_50CALPrice);
                    }
                }
            }
        }
    }

    private void AmountOfWeapons()
    {
        m_AKText.m_String = m_AKText.m_StringWhenStarted + ": " + m_AKList.Count;
        m_GlockText.m_String = m_GlockText.m_StringWhenStarted + ": " + m_GlockList.Count;
        m_SPASText.m_String = m_SPASText.m_StringWhenStarted + ": " + m_SPASList.Count;
        m_SKSText.m_String = m_SKSText.m_StringWhenStarted + ": " + m_SKSList.Count;
        m_LMGText.m_String = m_LMGText.m_StringWhenStarted + ": " + m_LMGList.Count;
        m_RemingtonText.m_String = m_RemingtonText.m_StringWhenStarted + ": " + m_RemingtonList.Count;
        m_BoxKnifeText.m_String = m_BoxKnifeText.m_StringWhenStarted + ": " + m_BoxKnifeList.Count;
        m_Kar98kText.m_String = m_Kar98kText.m_StringWhenStarted + ": " + m_Kar98kList.Count;
        m_ZombieKnifeText.m_String = m_ZombieKnifeText.m_StringWhenStarted + ": " + m_ZombieKnifeList.Count;
        m_GrenadeText.m_String = m_GrenadeText.m_StringWhenStarted + ": " + m_GrenadeList.Count;
        m_CrossbowText.m_String = m_CrossbowText.m_StringWhenStarted + ": " + m_CrossbowList.Count;
        m_50CALText.m_String = m_50CALText.m_StringWhenStarted + ": " + m_50CALList.Count;
    }
}
